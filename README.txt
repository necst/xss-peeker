===========================================================================================================================================================
		XssPeeker												12/02/2014
===========================================================================================================================================================

[Description]

XssPeeker is a study of black-box web vulnerability scanners with respect to XSS vulnerabilities.
We developed a semi-automatic methodology to thoroughly analyze how several commercial and open-source scanners work.
This includes:
 an automatic payload extractor for each available scanner;
 an automatic templating algorithm to cluster extracted payloads, to scale down the evaluation challenge and keep it feasible;
 an automatic process to extract informations used to evaluate generated templates;
 a publicly available test bed web application that exposes non trivial XSS vulnerabilities, improved and augmented by adding new cases based on 
scanners tests (http://public-firing-range.appspot.com).

In this repository we stored materials and scripts needed to perform all steps of our approach. This includes HTTP network traffic captured during scanner'
tests against our web application (pcap format), scanner reports generated at the end of a test scan and python scripts to parse and process the previous 
materials.


This project has been developed on a PC mounting Debian 6.0.7 squeeze.


[Employed software versions]

Python 2.6.6

libxml2 2.9.1

libxslt 1.1.28

pynids 0.6.1

dpkt 1.8

java version "1.6.0_18"
OpenJDK Runtime Environment (IcedTea6 1.8.13) (6b18-1.8.13-0+squeeze2)
OpenJDK Client VM (build 14.0-b16, mixed mode, sharing)

wireshark 1.2.11

libcap 1.1.1

gcc 4.4.5

urllib 1.17

chardet 2.1.1

nlk 2.0.4

numpy 1.7.1

pcapy 0.10.6

python_Levenshtein 0.10.2



[Scanners versions]

Acunetix 8.0
Netsparker 3.0.15.0
NStalker 10.13.11.28
NTOSpider 6.0.729
Skipfish 2.10b
w3af 1.2



[Structure]

XSS Peeker is composed by two main folders. "payloadsProcessing" contains all scripts and materials concerning works on payloads: payloads extraction,
templates generation and evaluation and captured network traffic between scanners and application. "reportParse" contains examples of reports, generated
by scanners after finishing tests on our test application, and a script to extract useful informations from them.


1)payloadsProcessing

The "dump" folder contains examples of network traffic between scanners and the web application, captured with wireshark and saved in pcap format. In order
 for scripts to be able to find and parse new dumps, they have to be named as shown in the examples (i.e., "dumpFiring"+scanner name+".pcap") and must have 
same folder path.

"payloads.py" parses pcap dumps using dpkt (python library) and extracts employed payloads from scanner's test requests. main() receives as input the 
name of scanner, to get its pcap from the "dump" folder, and returns the list of extracted payloads and the list of all GET and POST parsed requests.

"xssTemplating.py" generates templates for all payloads extracted from a scanner's requests by payloads.py. main() receives as input the name of a scanner,
calls payloads.py to receive extracted payloads from that scanner and returns a dictionary of templates (having as key generated templates and as value
their regex equivalent, where placeholders are substituted with appropriate patterns) and the list of extracted payloads free of site-dependent content.

"templEvaluation.py" computes metrics used for templates evaluation. When executed, cycles through the list of all available scanners and for each calls 
xssTemplating.py to gather the list of its templates. Outputs a text file displaying evaluation metrics for every generated template.

"filterEvasion.py" When executed, cycles through the list of all available scanners and analyzes payloads and templates of each scanner to detect attempts 
to evade xss filters. Outputs a text file displaying for all payloads employed evasion techniques.

"tcpAssembly.py" was implemented to create a finer way to deal with a corner case we met: we noticed that some scanners' POST requests were not parsed
correctly by dpkt and had their headers separated from their bodies, which were treated as separated packets. By analyzing the TCP dump with wireshark, we 
discovered that these requests had been split into separated TCP segments and, since dpkt does not perform TCP segments reassembly, could not be parsed as a
whole packet. This approach however yielded worse results than the original one, implemented inside payloads.py, and was left out in the final version.

"payloads2.py" is a different version of payloads.py, modified in order to call tcpAssembly.py to deal with the fusion of separated packets.


2)reportParse
Since each scanner produces a different report (varying in file extension and structure), we had to employ custom extraction procedures for each scanner
report.

"json" folder contains inputs and outputs for Skipfish reports. Skipfish produces reports in html format. However, we noticed that this html page is 
dynamically generated and informations about discovered vulnerabilities are taken from a json file. Therefore, we decided to directly extract and parse this
json file contained in the output folder created during a scan session.

"pdf" folder contains inputs and outputs for NStalker reports. Since NStalker only generates reports in pdf format, we decided to convert them in html 
format first (executing "pdftohtml -i -f 5 -l 13 NStalkerReport.pdf
" command) and then parse the newly created html page.

"xml" contains inputs and outputs concerning reports in xml format, produced by Acunetix, NetSparker, NTOSpider and w3af. Output files showing extracted 
informations are saved in this folder. Subfolder "reports" contains examples of xml reports produced by scanners.

"reportParse.py" parses reports from scanners and extracts positive payloads (i.e. payloads that trigged a vulnerability allowing to discover it).
Receives as input the name of a scanner to get its report from the right folder and returns the list of positive payloads.


[How to run]

Our project supports 6 web application scanners: Acunetix 8.0, Netsparker 3.0.15.0, NStalker 10.13.11.28, NTOSpider 6.0.729, Skipfish 2.10b, w3af 1.2

The execution of ./templEvaluation.py will automatically perform all the steps, from payload extraction to template generation and evaluation.
The execution ./filterEvasion.py, used to analyze evasion techniques employed in scanners' payloads, must be performed separately.

After running these two scripts, results for each scanners will be stored in the corresponding folders.

Produced results in "payloadProcessing":

-cleanPayloads.txt: contains all scanner's payloads free of site-dependent content;

-MatchingStrings.txt: is an output file used to check if generated templates are able to match all extracted payloads;

-pathInjection.txt: reports path injections extraction steps;

-payloads.txt: contains all payloads extracted from scanner's requests;

-requests.txt: lists all test requests, showing for each the payload and the injected variable;

-stream.txt: shows the HTTP stream extracted from pcap files;

-tcpAssembly.txt: shows the tcp re-assembly process to re-assemble POST requests whose headers and body have been separated during packet parsing;

-templates.txt: shows all steps to generate templates from original payloads;

-templatesEvaluation.txt: reports the evaluation of each generated template.


Produced results in "reportParse":

Results obtained by parsing a scanner's report are stored in the corresponding folder, depending on the format of the generated report.

===========================================================================================================================================================

AUthor: Enrico Bazzoli		e-mail: enrico.bazzoli@mail.polimi.it
#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

	
import json
import re
import urllib
import ast
import urlparse
import HTMLParser
import sys
import os
import re
from lxml import etree

def query_parse(requests, reportResults):
	payloads = []
	for req in requests:
		query = urlparse.parse_qs(req.encode('ASCII'))
		#query = urlparse.parse_qs(req)
		for key, value in query.items():
			val = str(value).strip("[]")
			if (val[0] == "'"):
				val = val.strip("\'")
			else:
				val = val.strip("\"")
			k = re.sub('AMPER', '&', re.sub('SEMI', ';', key))
			val = re.sub('AMPER', '&', re.sub('SEMI', ';', val))
			val = re.sub("\\\\'", "'", val)
			payloads.append(val)
			reportResults.write("Variable: " + k + "\n")
			reportResults.write("Value: " + val + "\n\n")

	reportResults.write("\n")
	return payloads

def main(path, inputFile):
	if inputFile == "Nstalker":
		fileOutput = path + "/pdf/NStalkerVulnreport.txt"
		reportResults = open(fileOutput, "w")
		fileInput = path + "/pdf/NStalkerReports.html"
		reportFile = open(fileInput, "r")
		reportText = str(reportFile.readlines())
		reportResults.write("Nstalker found vulnerabilities: \n\n")
		'''
		urls = []
		h = HTMLParser.HTMLParser()
		#url = str(re.findall('HTTP Request Evidence</b><br>\\\\n\', \'(.*?)&nbsp;<br>X-WebSecurityScanner', reportText, re.S))
		for url in re.findall('HTTP Request Evidence</b><br>\\\\n\', \'(.*?)&nbsp;<br>X-WebSecurityScanner', reportText, re.S):
			url = h.unescape(url)
			urls.append(url)
			reportResults.write("Url: " + url + "\n")
		'''
		testStrings = []
		#testString = str(re.findall('Connection: Keep-Alive&nbsp;<br>\\\\n\', \'(.*?);<br>\\\\n\', \'<b>HTTP Response Evidence', reportText, re.S))
		h = HTMLParser.HTMLParser()
		for testGetString in re.findall('<a href=\"(.*?)\">/reflected', reportText, re.S):
			testString = h.unescape(testGetString)
			testStrings.append(testGetString)
			reportResults.write("Test string: " + testGetString + "\n\n")

		payloads = query_parse(testStrings, reportResults)
		#for testPostString in re.findall('<b>Name</b><br>^(.*)<br>(.*)^<b>Value', reportText, re.M):
		reportResults.write("POST request payload:\n") 
		for testVariable in re.findall('<b>Name</b><br>\\\\n\', \'(.*)<br>\\\\n\', \'<b>Value</b><br>', reportText, re.M):
			reportResults.write("Variable: " + testVariable + "\n")
		for testValue in re.findall('<b>Value</b><br>\\\\n\', \'(.*?)<br>\\\\n', reportText, re.M):
			testValue = h.unescape(testValue)
			reportResults.write("Value: " + testValue.encode("utf-8") + "\n")
		if not testValue in payloads:
			payloads.append(testValue)

		
		return payloads
		
		reportResults.close()
		reportFile.close()		

	elif inputFile == "Skipfish":
		fileInput = path + "/json/samples.js"
		fileOutput1 = path + "/json/temp.js"
		fileOutput2 = path + "/json/skipfishVulnreport.txt"
		tempFile = open(fileOutput1, "w")
		reportResults = open(fileOutput2, "w")

		flag = 0
		jsonString = ""
		with open(fileInput) as jsonFile:
			for line in jsonFile:
				if "issue_samples" in line:
					flag = 1
				if flag == 1:
					jsonString = jsonString + line

		data = jsonString.replace("var ", "{'")
		data = data.replace(" =", "' :")
		count = data.count(";") - 1
		data = data.replace(";", "},", count).replace(";", "}",) 
		dump = json.dumps(data)
		tempFile.write(json.loads(dump))
		jdict = ast.literal_eval(json.loads(dump))
		queries = []
		reportResults.write("Skipfish found vulnerabilities: \n\n")
		i = 0
		while i < len(jdict['issue_samples']):
			if jdict['issue_samples'][i]['type'] == 40304:
				reportResults.write("XSS vector via arbitrary URLs\n")
				j = 0
				while j < len(jdict['issue_samples'][i]['samples']):
					url = jdict['issue_samples'][i]['samples'][j]['url']				
					reportResults.write("Request: " + url + "\n\n")

					parsed = urlparse.urlparse(url)
					query = parsed.query
					if not query in queries:
						queries.append(query)

					j = j + 1

			if jdict['issue_samples'][i]['type'] == 40101:
				reportResults.write("XSS vector in document body\n")
				j = 0
				while j < len(jdict['issue_samples'][i]['samples']):
					url = jdict['issue_samples'][i]['samples'][j]['url']
					reportResults.write("Request: " + url + "\n")
					
					parsed = urlparse.urlparse(url)
					query = parsed.query
					if not query in queries:
						queries.append(query)
					j = j + 1

			if jdict['issue_samples'][i]['type'] == 40105:
				reportResults.write("XSS vector via injected HTML tag attribute\n")
				j = 0
				while j < len(jdict['issue_samples'][i]['samples']):
					url = jdict['issue_samples'][i]['samples'][j]['url']
					reportResults.write("Request: " + url + "\n")
					
					parsed = urlparse.urlparse(url)
					query = parsed.query
					if not query in queries:
						queries.append(query)
					j = j + 1
			i = i + 1
	
		reportResults.write("\n\n")
		payloads = query_parse(queries, reportResults)

		jsonFile.close()
		tempFile.close()

		return payloads
	
	else:

		fileInput = path + "/xml/reports/" + inputFile + "FiringReport.xml"
		folder =  path + "/xml/" + inputFile
		if not os.path.exists(folder):
		    		os.makedirs(folder)
		outFile = path + "/xml/" + inputFile + "/" + inputFile + "VulnReport.txt"
		reportResults = open(outFile, "w")

		tree = etree.parse(fileInput)
		root = tree.getroot()
		reportResults.write(inputFile + " found vulnerabilities: \n\n")

		if inputFile == "W3af":
			queries = []
			for element in root.iter("description"):
				description = (element.text).lstrip()	
				reportResults.write(description + "\n")
				#for match in re.findall("data was: \"(.*)\". The modified parameter was", description):
				for match in re.findall("data was: \"(.*)\". This vulnerability", description):
					if not match in queries:
						queries.append(match)
			payloads = query_parse(queries, reportResults)
			return payloads
		
		elif inputFile == "Acunetix":
			i = 0
			queries = []
			for element in root.iter():
				if element.tag == 'Request':
					i = i + 1
					#request = str(re.findall('^(.*?) HTTP/1.1', element.text)).strip("[']")
					for request in re.findall('^(.*?) HTTP/1.1', element.text):
						reportResults.write("Request: " + request + "\n")
					#testString = str(re.findall('\n\n(.*?)$', element.text)).strip("[']")
					for testString in re.findall('\n\n(.*?)$', element.text):
								
						if testString:			
							reportResults.write("Test string: " + testString + "\n\n")		
							testString = re.sub("%26", "AMPER", testString)
							testString = re.sub(';', 'SEMI', testString)

							if not testString in queries:
								queries.append(testString)
						else:
							reportResults.write("\n")
							request = re.sub("%26", "AMPER", request)
							request = re.sub(';', 'SEMI', request)
							parsed = urlparse.urlparse(request)
							if not parsed.query in queries:
								queries.append(parsed.query)

			payloads = query_parse(queries, reportResults)
			return payloads
		
		elif inputFile == "Netsparker":
			payloads = []
			for element in root.iter("vulnerability"):
				subIter = element.getiterator()
				attributes = str(element.attrib).strip("{}")
				reportResults.write(attributes + "\n")
				for i in subIter:
					if i.tag == "url":
						tag = str(i.tag)
						text = str(i.text)
						reportResults.write(tag + ": " + text + "\n")
					if i.tag == "vulnerableparametertype":
						text = str(i.text)
						reportResults.write("Request type: " + text + "\n")
					if i.tag == "vulnerableparameter":
						text = str(i.text)
						reportResults.write("Vulnerable parameter: " + text + "\n")
					if i.tag == "vulnerableparametervalue":
						text = str(i.text)
						reportResults.write("Parameter value: " + text + "\n\n")
						
						payloads.append(text)
			return payloads

		elif inputFile == "NTOSpider":
			payloads = []
			for element in root.iter("Vuln"):
				subIter = element.getiterator()
				for i in subIter:
					if i.tag == "VulnUrl":
						text = str(i.text)
						reportResults.write("Vulnerable URl: " + text + "\n\n")
					if i.tag == "ParameterName":
						text = str(i.text)
						reportResults.write("Vulnerable parameter: " + text + "\n\n")
					if i.tag == "AttackValue":
						text = str(i.text)
						reportResults.write("Parameter value: " + text + "\n\n")
						payloads.append(text)
			return payloads



#!/usr/bin/python
# -*- coding: iso-8859-15 -*-


import dpkt
import urllib
import urlparse
import os
import re
import pdb
import tcpAssembly

def special_strings_Acunetix(request, out, payloads, pathInj, testRequests):
	#Appends special test strings used by Acunetix to the payloads list
	url = re.sub("%26", "AMPER", request.uri)
	body = re.sub("%26", "AMPER", request.body)
	#url = re.sub(';', 'SEMI', urllib.unquote_plus(url).encode('string_escape'))
	#body = re.sub(';', 'SEMI', urllib.unquote_plus(body).encode('string_escape'))
	url = re.sub(';', 'SEMI', url)
	body = re.sub(';', 'SEMI', body)
	stringInURL = 0
	stringInBody = 0
	
	#if ("acu" in url.lower() or "\(\):;" in url.lower() or "acu" in body.lower() or "\(\):;" in body.lower()):
	if ("acu" in url.lower() or "%40" in url.lower()):
		stringInURL = 1
	if ("acu" in body.lower() or "%40" in body.lower()):
		stringInBody = 1
	if(stringInBody or stringInURL):
		out.write(request.method + "\n")
		out.write("URL: " + request.uri + "\n")
		out.write("Body: " + request.body + "\n")
		out.write("{\n")

		testRequests.append(request)
		if body:
			if stringInBody:
				query = urlparse.parse_qs(body)
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		else:
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)

		if not query:
			if not url in pathInj:
				pathInj.append(url)
		for key, value in query.items():
			val = str(value).strip("[]")
			if (val[0] == "'"):
				val = val.strip("\'")
			else:
				val = val.strip("\"")
			val = re.sub('AMPER', '&', re.sub('SEMI', ';', val))
			out.write(key + "=" + val + "\n")
			if("acu" in val.lower() or "%40" in val.lower()):
				if not val in payloads:
					payloads.append(val)
		query = re.sub('SEMI', ';', str(query))
		query = re.sub('AMPER', '&', query)
		out.write("Query: " + query + "\n")
		out.write("}\n\n")
		

#def special_strings_Appscan(request):
#Appscan doesn't seem to have special strings so I would just exclude it from this tests
	
def special_strings_Netsparker(request, out, payloads, pathInj, testRequests):
	#Appends special test strings used by NetSparker to the payloads list
	url = re.sub("%26", "AMPER", request.uri)
	body = re.sub("%26", "AMPER", request.body)
	url = re.sub("%23", "HASH", url)
	url = re.sub(';', 'SEMI', urllib.unquote_plus(url))
	body = re.sub(';', 'SEMI', urllib.unquote_plus(body))
	
	stringInURL = 0
	stringInBody = 0
	
	if("netsparker" in url.lower()):
		stringInURL = 1
	if("netsparker" in body.lower()):
		stringInBody = 1
	if(stringInBody or stringInURL):
		out.write(request.method + "\n")
		out.write("URL: " + request.uri + "\n")
		out.write("Body: " + request.body + "\n")
		out.write("{\n")
		testRequests.append(request)
		if body:
			if stringInBody:
				query = urlparse.parse_qs(body)
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		else:
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		if not query:
			if not url in pathInj:
				pathInj.append(url)
		for key, value in query.items():
			val = str(value).strip("[]")
			if (val[0] == "'"):
				val = val.strip("\'")
			else:
				val = val.strip("\"")
			#togliere
			out.write("Prova: " + val + "\n")
			#
			val = re.sub('AMPER', '&', re.sub('SEMI', ';', re.sub('HASH', '#', val)))
			out.write(key + "=" + val + "\n")
			if("netsparker" in val.lower()):
				if not val in payloads:
					payloads.append(val)
		query = re.sub('SEMI', ';', str(query))
		query = re.sub('AMPER', '&', query)
		query = re.sub('HASH', '#', query)
		out.write("Query: " + query + "\n")
		out.write("}\n\n")

def special_strings_Nstalker(request, out, payloads, pathInj, testRequests):
	#Appends special test strings used by Nstalker to the payloads list
	url = re.sub("%26", "AMPER", request.uri)
	body = re.sub("%26", "AMPER", request.body)
	url = re.sub(';', 'SEMI', urllib.unquote_plus(url))
	body = re.sub(';', 'SEMI', urllib.unquote_plus(body))
	stringInURL = 0
	stringInBody = 0
	if ("nstalker" in url.lower()):
		stringInURL = 1
	if("nstalker" in body.lower()):
		stringInBody = 1
	if(stringInBody or stringInURL):
		out.write(request.method + "\n")
		out.write("URL: " + request.uri + "\n")
		out.write("Body: " + request.body + "\n")
		out.write("{\n")
	
		testRequests.append(request)
		if body:
			if stringInBody:
				query = urlparse.parse_qs(body)
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		else:
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		if not query:
			if not url in pathInj:
				pathInj.append(url)
		for key, value in query.items():
			val = str(value).strip("[]")
			if (val[0] == "'"):
				val = val.strip("\'")
			else:
				val = val.strip("\"")
			val = re.sub('AMPER', '&', re.sub('SEMI', ';', val))
			out.write(key + "=" + val + "\n")
			if("nstalker" in val.lower()):
				if not val in payloads:
					payloads.append(val)
		query = re.sub('SEMI', ';', str(query))
		query = re.sub('AMPER', '&', query)
		out.write("Query: " + query + "\n")
		out.write("}\n\n")
	
def special_strings_NTOSpider(request, out, payloads, pathInj, testRequests):#
	#Appends special test strings used by NTOSpider to the payloads list
	url = re.sub("%26", "AMPER", request.uri)
	body = re.sub("%26", "AMPER", request.body)
	url = re.sub(';', 'SEMI', urllib.unquote_plus(url))
	body = re.sub(';', 'SEMI', urllib.unquote_plus(body))
	stringInURL = 0
	stringInBody = 0
	if ("xe" in url.lower()):
		stringInURL = 1
	if("xe" in body.lower()):
		stringInBody = 1
	if(stringInBody or stringInURL):
		out.write(request.method + "\n")
		out.write("URL: " + request.uri + "\n")
		out.write("Body: " + request.body + "\n")
		out.write("{\n")
	
		testRequests.append(request)
		if body:
			if stringInBody:
				query = urlparse.parse_qs(body)
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		else:
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		if not query:
			if not url in pathInj:
				pathInj.append(url)
		for key, value in query.items():
			val = str(value).strip("[]")
			if (val[0] == "'"):
				val = val.strip("\'")
			else:
				val = val.strip("\"")
			val = re.sub('AMPER', '&', re.sub('SEMI', ';', val))
			out.write(key + "=" + val + "\n")
			if("xe" in val.lower()):
				if not val in payloads:
					payloads.append(val)
		query = re.sub('SEMI', ';', str(query))
		query = re.sub('AMPER', '&', query)
		out.write("Query: " + query + "\n")
		out.write("}\n\n")


def special_strings_Skipfish(request, out, payloads, pathInj, testRequests):
	#Appends special test strings used by Skipfish to the payloads list
	url = re.sub("%26", "AMPER", request.uri)
	body = re.sub("%26", "AMPER", request.body)
	url = re.sub(';', 'SEMI', urllib.unquote_plus(url))
	body = re.sub(';', 'SEMI', urllib.unquote_plus(body))
	stringInURL = 0
	stringInBody = 0
	if ("sfi" in url.lower()):
		stringInURL = 1
	if("sfi" in body.lower()):
		stringInBody = 1
	if(stringInBody or stringInURL):
		out.write(request.method + "\n")
		out.write("URL: " + request.uri + "\n")
		out.write("Body: " + request.body + "\n")
		out.write("{\n")
	
		testRequests.append(request)
		if body:
			if stringInBody:
				query = urlparse.parse_qs(body)
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		else:
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		if not query:
			if not url in pathInj:
				pathInj.append(url)
		for key, value in query.items():
			val = str(value).strip("[]")
			if (val[0] == "'"):
				val = val.strip("\'")
			else:
				val = val.strip("\"")
			val = re.sub('AMPER', '&', re.sub('SEMI', ';', val))
			out.write(key + "=" + val + "\n")
			if("sfi" in val.lower()):
				if not val in payloads:
					payloads.append(val)
		query = re.sub('SEMI', ';', str(query))
		query = re.sub('AMPER', '&', query)
		out.write("Query: " + query + "\n")
		out.write("}\n\n")

def special_strings_W3af(request, out, payloads, pathInj, testRequests):#
	#Appends special test strings used by W3af to the payloads list
	url = re.sub("%26", "AMPER", request.uri)
	body = re.sub("%26", "AMPER", request.body)
	url = re.sub(';', 'SEMI', urllib.unquote_plus(url))
	body = re.sub(';', 'SEMI', urllib.unquote_plus(body))	

	stringInURL = 0
	stringInBody = 0
	if(("fake_alert" in url.lower()) or ("XDBO" in url.lower()) or ("%3cum" in url.lower())):
		stringInURL = 1
	if(("fake_alert" in body.lower()) or ("XDBO" in body.lower()) or ("%3cum" in body.lower())):
		stringInBody = 1
	if(stringInBody or stringInURL):
		out.write(request.method + "\n")
		out.write("URL: " + request.uri + "\n")
		out.write("Body: " + request.body + "\n")
		out.write("{\n")
	
		testRequests.append(request)
		if body:
			if stringInBody:
				query = urlparse.parse_qs(body)
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		else:
			if stringInURL:
				parsed = urlparse.urlparse(url)
				query = urlparse.parse_qs(parsed.query)
		if not query:
			if not url in pathInj:
				pathInj.append(url)
		for key, value in query.items():
			val = str(value).strip("[]")
			if (val[0] == "'"):
				val = val.strip("\'")
			else:
				val = val.strip("\"")
			val = re.sub('AMPER', '&', re.sub('SEMI', ';', val))
			out.write(key + "=" + val + "\n")
			if("fake_alert" in val.lower() or "XDBO" in val.lower() or "%3cum" in val.lower()):
				if not val in payloads:
					payloads.append(val)
		query = re.sub('SEMI', ';', str(query))
		query = re.sub('AMPER', '&', query)
		out.write("Query: " + query + "\n")
		out.write("}\n\n")

	
def parseQueryString(query, url, pathInj, payloads, out, payloads_dict, input1):
	
	if not query:
		if not url in pathInj:
				pathInj.append(url)
		return

	for key, value in query.items():
		
		val = str(value).strip("[]")
		if (val[0] == "'"):
			val = val.strip("\'")
		else:
			val = val.strip("\"")
		val = re.sub('AMPER', '&', re.sub('SEMI', ';', val))
		out.write(key + " = " + val + "\n")
		if("<" in val or ">" in val or "\"" in val or "\'" in val or "`" in val):
			if not val in payloads:
				payloads_dict [ val ] = url

				payloads.append(val)

def main(input1):
	#input1 = "Acunetix"
	#input1 = "Appscan"
	#input1 = "Netsparker"
	#input1 = "Nstalker"
	#input1 = "Skipfish"
	#input1 = "W3af"

	#input1 = raw_input('Name of first scanner: \n')

	file1 = "dump/dumpFiring" + input1 + ".pcap"

	f = open(file1, 'rb')
	
	print file1
	i = 0
	j = 0


	fileName1 = input1 + "/requests.txt"
	fileName2 = input1 + "/payloads.txt"
	fileName3 = input1 + "/pathInjections.txt"
	
	out = open(fileName1, "w")
	out2 = open(fileName2, "w")
	out3 = open(fileName3, "w")

	fileName4 = input1 + "/prova.txt"
	out4 = open(fileName4, "w")
	fileName5 = input1 + "/stream.txt"
	out5 = open(fileName5, "w")

	pathInj = []
	payloads = []
	payloads_dict = dict()
	testRequests = []
	allRequests = []
	results = []
	out.write(file1 + ": \n\n")
	
	#calls tcpAssembly to deal with reassembly of tcp segments
	requests = tcpAssembly.main(file1)
	j = len(requests)
		
	for http in requests:

		out4.write("\nRichiesta accettata:\n")
		out4.write(str(http) + "\n\n")	
	
		if (http.method == "POST" or http.method == "GET"):
		
			#manual escaping of ";" and "%26" (url encoding of "&") to make sure that the query parser doesn't split
			#the value of the variable
			url = re.sub("%26", "AMPER", http.uri)
			body = re.sub("%26", "AMPER", http.body)
			url = re.sub(';', 'SEMI', urllib.unquote_plus(url))
			body = re.sub(';', 'SEMI', urllib.unquote_plus(body))
			allRequests.append(http)
			stringInURL = 0
			stringInBody = 0

		
			if input1 == "Acunetix":
				#this block solves the problem of special characters not correctly encoded
				url = url.encode('string_escape')
				body = body.encode('string_escape')
				url = url.decode('string_escape')
				body = body.decode('string_escape')

				special_strings_Acunetix(http, out, payloads, pathInj, testRequests)
			
			elif input1 == "Netsparker":
				special_strings_Netsparker(http, out, payloads, pathInj, testRequests)
			
			elif input1 == "Nstalker":
				special_strings_Nstalker(http, out, payloads, pathInj, testRequests)
			
			elif input1 == "Skipfish":
				special_strings_Skipfish(http, out, payloads, pathInj, testRequests)
			
			elif input1 == "W3af":
				special_strings_W3af(http, out, payloads, pathInj, testRequests)

			elif input1 == "NTOSpider":
				special_strings_NTOSpider(http, out, payloads, pathInj, testRequests)
			

			if ((url.lower().find('%3c') !=-1) or (url.lower().find('%3e') !=-1) or (url.find('%27') !=-1) or (url.find('%22') !=-1) or (url.find("\'") !=-1) or (url.find('\"') !=-1) or (url.find('<') !=-1) or (url.find('>') !=-1) or (url.find('`') !=-1)):
				stringInURL = 1

			if(body.find('<') !=-1 or body.find('>') !=-1 or body.find('\'') !=-1 or body.find('\"') !=-1 or body.lower().find('%3c') !=-1 or body.lower().find('%3e') !=-1 or body.find('%27') !=-1 or body.find('%22') !=-1 or body.find('`') !=-1):
				stringInBody = 1
		
			if(stringInBody or stringInURL):
				out.write(http.method + "\n")
				testRequests.append(http)
				out.write("URL: " + http.uri + "\n")
				out.write("Body: " + http.body + "\n")
				out.write("{\n")
				if body:
					if stringInBody:
						query = urlparse.parse_qs(body)
						parseQueryString(query, http.body, pathInj, payloads, out, payloads_dict, input1)
					if stringInURL:
						parsed = urlparse.urlparse(url)
						query = urlparse.parse_qs(parsed.query)
						parseQueryString(query, http.uri, pathInj, payloads, out, payloads_dict, input1)
				else:
					if stringInURL:
						parsed = urlparse.urlparse(url)
						query = urlparse.parse_qs(parsed.query)
						parseQueryString(query, http.uri, pathInj, payloads, out, payloads_dict, input1)
				query = re.sub('SEMI', ';', str(query))
				query = re.sub('AMPER', '&', query)
				out.write("Query: " + query + "\n")

				out.write("}\n\n")
		

	out3.write(file1 + ": \n\n")
	for injection in pathInj:
		inj = urllib.unquote_plus(injection)
		inj = re.sub('SEMI', ';', inj)
		inj = re.sub('AMPER', '&', inj)
		
	
		if input1 == "Acunetix":
			out3.write("\nOriginal: " + injection + "\n")
			testStrings = []
			endSplit = 0
			path = injection
			while not endSplit:
				split = os.path.split(path)
				if(split[1].find('<') !=-1 or split[1].find('>') !=-1 or split[1].lower().find('%3c') !=-1 or split[1].lower().find('%3e') !=-1 or split[1].find('%27') !=-1 or split[1].find('%22') !=-1 or split[1].find('"') !=-1 or "acu" in split[1].lower() or "%40" in split[1]):
					section = split[1]
					#if the slash is part of a tag (e.g.: </script>)
					if split[0][-1] == "<":
						section = split[0] + "SLASH" + split[1]
						decodedString = re.sub('SLASH', "/", urllib.unquote_plus(section).encode('string_escape'))
						out3.write("Modifica: " + decodedString + "\n")
						path = section
						continue
						
					testStrings.append(section)
					decodedString = re.sub('SLASH', "/", urllib.unquote_plus(section).encode('string_escape'))
					out3.write("Modifica: " + decodedString + "\n")
				path = split[0]
				if len(path) < 4:
					endSplit = 1

			for t in testStrings:
				decodedString = urllib.unquote_plus(t).encode('string_escape')
				decodedString = re.sub('SEMI', ';', decodedString)
				decodedString = re.sub('AMPER', '&', decodedString)
				decodedString = re.sub('SLASH', "/", decodedString)
				'''
				if decodedString.find('?') != -1:
					ind = decodedString.find('?')
					decodedString = decodedString[ind+1:len(decodedString)]
					out3.write("Modifica: " + decodedString + "\n")
				'''
				if not any(decodedString in p for p in payloads):
					payloads.append(decodedString)
					payloads_dict [ inj ] = injection

		if input1 == "Appscan":
			out3.write(inj + "\n")
			ind = inj.find('/')
			testString = inj[ind+1:len(inj)]
			if testString.find('?') != -1:
				ind2 = testString.find('?')
				testString = testString[ind2+1:len(testString)]

			if not any(testString in p for p in payloads):
				payloads.append(testString)
				payloads_dict [ inj ] = injection


		if input1 == "Netsparker":
			#netsparker uses double url encoding for some strings so I double decode only for this scanner
			inj = urllib.unquote_plus(inj)
			noslash = 1

			out3.write("\nOriginal1: " + inj + "\n")

			if inj[len(inj)-1] == "/":
				testString = inj[:-1]
			else:
				testString = inj
			ind = testString.find('/')
			testString = testString[ind+1:len(testString)]

			out3.write("Original2: " + testString + "\n")

			if testString.find('?') != -1:
				ind2 = testString.find('?')
				testString = testString[ind2+1:len(testString)]
				out3.write("Modifica ?: " + testString + "\n")
			
			elif testString.find('\'') != -1:
				ind2 = testString.find('\'')
				testString = testString[ind2:len(testString)]
				out3.write("Modifica ': " + testString + "\n")
			
			elif testString.find('/') != -1:
				while noslash:
					if testString.find('/') == -1:
						noslash = 0
					else:
						ind2 = testString.find('/')
						if ind2 > 0 and (testString[ind2-1] == "<" or (len(testString)>(ind2+2) and testString[ind2+1] == "*" and testString[ind2+2] == "*") or testString[ind2-1] == "*"):
							noslash = 0
						else:
							testString = testString[ind2+1:len(testString)]
							out3.write("Modifica: " + testString + "\n")

			if not any(testString in p for p in payloads):
				payloads.append(testString)
				payloads_dict [ inj ] = injection

		if input1 == "Nstalker":
			out3.write("\nOriginal: " + inj + "\n")
			if inj.find('?') != -1:
				ind = inj.find('?')
				testString = inj[ind+1:len(inj)]
				if not any(testString in p for p in payloads):
					out3.write("Modifica: " + testString + "\n")
					payloads.append(testString)
					payloads_dict [ inj ] = injection
		
		if input1 == "NTOSpider":
			out3.write("\nOriginal: " + inj + "\n")
			if inj.find('?') != -1:
				ind = inj.find('?')
				testString = inj[ind+1:len(inj)]
				if not any(testString in p for p in payloads):
					out3.write("Modifica: " + testString + "\n")
					payloads.append(testString)
					payloads_dict [ inj ] = injection
		
		if input1 == "Skipfish":
			out3.write("\nOriginal: " + inj + "\n")
			testStrings = []

			endSplit = 0
			path = injection
			while not endSplit:
				split = os.path.split(path)
				if(split[1].find('<') !=-1 or split[1].find('>') !=-1 or split[1].lower().find('%3c') !=-1 or split[1].lower().find('%3e') !=-1 or split[1].find('%27') !=-1 or split[1].find('%22') !=-1 or split[1].find('"') !=-1 or split[1].find("'") !=-1 or "sfi" in split[1]):
					testStrings.append(split[1])
				path = split[0]
				if len(path) == 1:
					endSplit = 1
					
			for t in testStrings:
				out3.write("Modifica: " + t + "\n")
				if not any(t in p for p in payloads):
					payloads.append(t)
					payloads_dict [ inj ] = injection

		if input1 == "W3af":
			out3.write("\nOriginal: " + injection + "\n")
			
			if inj[0] == "/":
				testString = inj[1:]
			else:
				testString = inj
			
			ind = testString.find('/')
			testString = testString[ind+1:len(testString)]

			out3.write("Original2: " + testString + "\n")
			if not any(testString in p for p in payloads):
				payloads.append(testString)
				payloads_dict [ inj ] = injection
			

		#NTOSpider doesn't perform usefull path injections

	out2.write(file1 + ": \n\n")
	for p in payloads:
		out2.write(p + "\n")
	'''
	out4.write(file1 + ": \n\n")
	for key, value in payloads_dict.items():
		out4.write("Payload: " + key + "\n")
		out4.write("Original URL: " + str(value) + "\n\n")
	'''
	print j
	#print i
	
	f.close()
	out.close()
	out2.close()
	out3.close()
	out4.close()
	return payloads, allRequests

'''
inputFiles = ["Netsparker", "W3af", "Acunetix", "Nstalker", "Skipfish", "NTOSpider"]
for inputFile in inputFiles:
	main(inputFile)
'''
main("Nstalker")


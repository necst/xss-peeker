#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
	
import dpkt
import difflib
import payloads
import os
import re
import Levenshtein	

#checks if the total length of the matching blocks changes when switching c1 and c2, if it does it keeps the order that yields the highest length
def match_length(string1, string2):
	matchLen1 = 0
	matchLen2 = 0
	
	seq1 = difflib.SequenceMatcher(None, string1, string2)
	matchingBlocks1 = seq1.get_matching_blocks()
	for m in matchingBlocks1:
		matchLen1 = matchLen1 + m[2]

	seq2 = difflib.SequenceMatcher(None, string2, string1)
	matchingBlocks2 = seq2.get_matching_blocks()
	for m in matchingBlocks2:
		matchLen2 = matchLen2 + m[2]
	
	if matchLen1 >= matchLen2:
		return seq1, 0
	else:
		return seq2, 1

#checks to make sure that there are no overlapping templates
def templates_cleaner(templates, out, inputfile):
	shortStrings = []
	for r in templates:
		temp1 = re.sub('(-STR-)+', '', r)
		temp2 = re.sub('(_NUM_)+', '', temp1)
		if len(temp2) < 4:
			shortStrings.append(r)

	for s in shortStrings:
		templates.remove(s)

	reducedTemplates = []
	for t in templates:
		temp = re.sub('(-STR-)+', '-STR-', t)
		temp1 = re.sub('(_NUM_)+', '_NUM_', temp)
		if not(temp1 in reducedTemplates):
			reducedTemplates.append(temp1)
	
	templates = reducedTemplates
	
	overlapping = []
	#block for STR
	for t1 in templates[:len(templates)-1]:
		for t2 in templates[templates.index(t1)+1:]:
			temp1 = re.sub('-STR-', '', t1)
			temp2 = re.sub('-STR-', '', t2)
			
			if temp1 == temp2:
				if len(t1) > len(t2):
					overlapTemp = t2
				else:
					overlapTemp = t1
				if not overlapTemp in overlapping:
					overlapping.append(overlapTemp)
					
	#block to remove Netsparker's troublesome strings
	for t in templates:
		if inputfile == "Netsparker" and (len(t) > 40 and (not("NUM" in t) or not("STR" in t))):
			if not t in overlapping:
				overlapping.append(t)

	for o in overlapping:
		templates.remove(o)

	return templates

#function responsible for creating templates out of samples
#input parameters: list1, list2, out file, min length of a matching block, max Levenshtein distance, cycle nr, min Levenshtein distance from the template (where "STR" is substituted with "*" and "_NUM_" with "#") to a string of the same length entirely composed by "*" or "#" (not used anymore)
def templating_function(list1, list2, out, minLen, maxLeven, cycle, bulletSharpDist):
	templates = []
	accepted = []
	rejected = []

	for c1 in list1[:len(list1)-1]:
		for c2 in list2[list2.index(c1)+1:]:
	

			if (Levenshtein.distance(c1, c2) < maxLeven):
				seq, switch = match_length(c1, c2)
				if switch:
					temp = c1
					c1 = c2
					c2 = temp
				matchingBlocks = seq.get_matching_blocks()
				generic = ""				
				i = 0
				totLen = []
				for match in matchingBlocks[:-1]:
					ind1 = match[0]
					ind2 = match[1]
					length = match[2]
					totLen.append(length)
					matchingBlock = c1[ind1:ind1+length]
					
					if length <= minLen:
						break
					if i == 0:
						if ind1 != 0 or ind2 != 0:

							nonMatchingBlock1 = c1[:ind1]
							nonMatchingBlock2 = c2[:ind2]
							
							#I commented comparisons between numbers and empty strings because of incorrect behavior. Needs to be fixed
							if (((nonMatchingBlock1).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock1)) or (nonMatchingBlock1 == "")) and (((nonMatchingBlock2).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock2))or (nonMatchingBlock2 == "")):
								generic = generic + "_NUM_"
								'''
							elif ((((nonMatchingBlock1).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock1))) and (nonMatchingBlock2 == "")) or (((nonMatchingBlock2).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock2)) and (nonMatchingBlock1 == "")):
								generic = generic + ""
								'''
							else:
								generic = generic + "-STR-"

						generic = generic + matchingBlock
					else:
						lastInd1 = matchingBlocks[i-1][0] + matchingBlocks[i-1][2]
						lastInd2 = matchingBlocks[i-1][1] + matchingBlocks[i-1][2]

						nonMatchingBlock1 = c1[lastInd1:ind1]
						nonMatchingBlock2 = c2[lastInd2:ind2]
						
						if (((nonMatchingBlock1).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock1))or (nonMatchingBlock1 == "")) and (((nonMatchingBlock2).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock2))or (nonMatchingBlock2 == "")):
							generic = generic + "_NUM_" + matchingBlock
							'''
						elif (((nonMatchingBlock1).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock1)) and (nonMatchingBlock2 == "")) or (((nonMatchingBlock2).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', nonMatchingBlock2)) and (nonMatchingBlock1 == "")):
							generic = generic + "" + matchingBlock
							'''
						else:
							generic = generic + "-STR-" + matchingBlock
					i = i + 1
					if i == len(matchingBlocks)-1:
						diff1 = len(c1) - (ind1 + length)
						diff2 = len(c2) - (ind2 + length)
						if diff1 != 0 or diff2 != 0:

							if (((c1[ind1 + length:]).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', c1[ind1 + length:])) or (c1[ind1 + length:] == "")) and (((c2[ind2 + length:]).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', c2[ind2 + length:])) or (c2[ind2 + length:] == "")):
								generic = generic + "_NUM_"
								'''
							if (((c1[ind1 + length:]).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', c1[ind1 + length:]))) and (((c2[ind2 + length:]).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', c2[ind2 + length:]))):
								generic = generic + "_NUM_"
							elif (((c1[ind1 + length:]).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', c1[ind1 + length:])) and (c2[ind2 + length:] == "")) or (((c2[ind2 + length:]).isdigit()) or (re.search('([0-9]*)(_NUM_)([0-9]*)', c2[ind2 + length:])) and (c1[ind1 + length:] == "")):
								generic = generic + ""
								'''
							else:
								generic = generic + "-STR-"
						
						if (not c1 in accepted):
							accepted.append(c1)
						if (not c2 in accepted):
							accepted.append(c2)
						
						if len(generic) > 5 and (not (generic in templates)):
							tempLen = len(generic)
							genericBullet = re.sub('-STR-', '*', generic)
							genericSharpBullet = re.sub('_NUM_', '#', genericBullet)
							bulletString = "*" * tempLen
							sharpString = "#" * tempLen
							if (Levenshtein.distance(genericSharpBullet, bulletString) > bulletSharpDist and Levenshtein.distance(genericSharpBullet, sharpString) > bulletSharpDist):
								templates.append(generic)
								out.write("Originals: \n" + c1 + "\n" + c2 + "\n")
								out.write("Matches Lengths: " + str(totLen) + "\n")
								out.write("Levenshtein: " + str(Levenshtein.distance(c1, c2)) + "\n")
								out.write("Leven Bullet/Sharp: " + str(Levenshtein.distance(genericSharpBullet, bulletString)) + " / " + str(Levenshtein.distance(genericSharpBullet, sharpString)) + "\n")
								out.write("Template: " + generic + "\n\n")
	
	#this block is needed to keep those strings that would have been discarded due to the lack of samples to compare with
	for l in list1:
		if not any(l in a for a in accepted):
			rejected.append(l)
	
	'''
	#this is an attempt to deal with payload such as Netsparkerc067f4f2eb9b42b5ad36d6783e99b0a5.html and Netsparker62af41d79b694e4790c6608d64db49c2 that normally would always fail the maxLeven check; this block takes payloads with same length among discarded ones and run them again with higher maxLeven. Currently NOT WORKING
	if not (cycle == 0.5):
		print "Cycle: ", cycle
		print "Retry"
		
		retry = []
		discarded = []
		print "Len rejected: ", len(rejected)
		for r1 in rejected[:len(rejected)-1]:
			for r2 in rejected[rejected.index(r1)+1:]:
				if len(r1) == len(r2):
					if not r1 in retry:
						retry.append(r1)
					if not r2 in retry:
						retry.append(r2)

		for r in rejected:
				if not (r in retry):
					discarded.append(r)
		print "Len discarded: ", len(discarded)
		for d in discarded:
			templates.append(d)

		print "Len retry: ", len(retry)
		rejected = []
		templatesRetry = templating_function(retry, retry, out, 0, 30, 0.5, 5)
		print "OK"
		templatesRetry = templates_cleaner(templatesRetry, out, inputFile)
		print "OK"
		for t in templatesRetry:
			templates.append(t)

	else:
		print "Cycle: ", cycle
		print "No Retry"
		for r in rejected:
			templates.append(r)
	'''
	
	
	for r in rejected:
			templates.append(r)
	return templates

def main (inputFile):

#decommented when data are needed for graphs
#def main (inputFile, oblivio):

	fileName1 = inputFile + "/templates.txt"
	fileName2 = inputFile + "/cleanPayloads.txt"
	fileName3 = inputFile + "/MatchingStrings.txt"

	out = open(fileName1, "w")
	out2 = open(fileName2, "w")
	out3 = open(fileName3, "w")
	
	payloadList = []
	requests = []
	payloadList, requests = payloads.main(inputFile)

	cleanPayloads = []
	badStrings = ["https://google.com", "toxicdom", "address", "form", "json", "index", "rangeCreateContextualFragment", "URL", "documentwrite", "cookie", "documentWrite", "document", "NOSTARTSWITHJS", ".html", "head", "location", "NOSTARTSWITHJS", "redirect", "filteredcharsets", "preseeded", "reflected", "SpaceDoubleQuoteSlashEquals", "https://iq-firingrange.appspot.com/", "tagname", "textarea", "css_style", "js_comment", "attribute_name", "attribute_script", "js_singlequoted_string", "DoubleQuoteSinglequote", "expression", "multiline", "attribute_quoted", "attribute_singlequoted", "body_comment", "parameter/"]

	for p in payloadList:
		dirtyString = p
		for b in badStrings:
			if b in dirtyString:
				dirtyString = dirtyString.replace(b, "")
		dirtyString = re.sub("\\\\'", "'", dirtyString)
		if dirtyString[0] == "?":
			dirtyString = dirtyString[1:]
		if not any(dirtyString in c for c in cleanPayloads):
			cleanPayloads.append(dirtyString)
		
	for c in cleanPayloads:
		out2.write(c + "\n")
	i = 0
	
	#custom parameter tuning for each scanner
	if inputFile == "Acunetix":
		maxLeven = 20
		oblivio = 0.9
	elif inputFile == "Netsparker":
		maxLeven = 15
		oblivio = 0.5
	elif inputFile == "NTOSpider":
		maxLeven = 15
		oblivio = 0.8
	elif inputFile == "Skipfish":
		maxLeven = 15
		oblivio = 0.9
	elif inputFile == "W3af":
		maxLeven = 15
		oblivio = 0.5
	#default case
	else:
		maxLeven = 20
		oblivio = 0.9
	
	minBlock = 1
	out.write("\nTemplates cycle " + str(i) + ": " + "\n\n")
	
	rawTemplates = templating_function(cleanPayloads, cleanPayloads, out, 2, maxLeven, i, 5)
	rawtemplates = templates_cleaner(rawTemplates, out, inputFile)
	
	#this block of code is de-commented when data for plotting benchmark graphs are needed
	'''
	oblivion = 0.0
	j = 1

	if not os.path.exists((inputFile + "/templGraphs")):
		os.makedirs((inputFile + "/templGraphs"))

	while oblivion <= 0.8:
		i = 0
		maxLeven = 15
		minBlock = 1
		
		plotFile = inputFile + "/templGraphs/plot" + str(j) + ".txt"
		plot = open(plotFile,  "w")
		oblivion = oblivion + 0.1
		plot.write("Oblivion: " + str(oblivion) + "\n")
		plot.write(str(maxLeven) + "\t" + str(len(cleanPayloads)) + "\n")
		j = j + 1
		finalTemplates = list(rawtemplates)
		while finalTemplates:
			i = i + 1
			minBlock = minBlock * oblivion
			maxLeven = round(maxLeven * oblivion)
			print "Lev ", maxLeven
			print "Block ", round(minBlock)
			out.write("\nTemplates cycle " + str(i) + ":\n\n")
			refinedTemplates = templating_function(finalTemplates, finalTemplates, out, round(minBlock), maxLeven, i, 5)
			refinedTemplates = templates_cleaner(refinedTemplates, out, inputFile)

			plot.write(str(maxLeven) + "\t" + str(len(refinedTemplates)) + "\n")

			if len(refinedTemplates) == len(finalTemplates):
				break
			finalTemplates = list(refinedTemplates)
	'''
	finalTemplates = list(rawtemplates)
	while finalTemplates:
		oblivion = oblivio
		i = i + 1
		minBlock = minBlock * oblivion
		maxLeven = round(maxLeven * oblivion)
		
		out.write("\nTemplates cycle " + str(i) + ":\n\n")
		refinedTemplates = templating_function(finalTemplates, finalTemplates, out, round(minBlock), maxLeven, i, 5)
		refinedTemplates = templates_cleaner(refinedTemplates, out, inputFile)

		if len(refinedTemplates) == len(finalTemplates):
			break
		finalTemplates = list(refinedTemplates)
		

	regExTemplates = []
	evaluationTemp = {}
	for r in finalTemplates:
		t1 = re.sub('-STR-', 'STR', r)
		t2 = re.sub('_NUM_', 'NUM', t1)
		temp = re.escape(t2)
		#temp1 = re.sub('(\\\\\*)+', '.*', temp)
		#temp2 = re.sub('(\\\\\#)+', '[0-9]*', temp1)
		temp1 = re.sub('STR', '.*', temp)
		temp2 = re.sub('NUM', '[0-9]*', temp1)
		evaluationTemp[t2] = temp2
		if temp2:
			if not (temp2 in regExTemplates):
				regExTemplates.append(temp2)
	out.write("\nTemplate RegEx form: \n\n")
	for r in regExTemplates:
		out.write(r + "\n")

	out.write("\nFinal templates: \n\n")
	for r in finalTemplates:	
		out.write(r + "\n")
		
	captured = []
	for r in regExTemplates:
		for line in cleanPayloads:
			for match in (re.findall(r, line)):
				if not(match in captured):
					out3.write(match + "\n")
					captured.append(match)

	out3.write("\nDiff payloads-captured: \n")
	for cl in cleanPayloads:
		if cl not in captured:
			out3.write(cl + "\n")

	out3.write("\nDiff captured-payloads: \n")
	for ca in captured:
		if ca not in cleanPayloads:
			out3.write(ca + "\n")

	out.close()
	out2.close()
	out3.close()
	return evaluationTemp, cleanPayloads


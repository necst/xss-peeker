#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import dpkt
import difflib
import os
import re

import xssTemplating
import payloads

#this script analyzes payloads and templates of each scanner to detect attempts to evade xss filters (as described by https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet)


inputFiles = ["Acunetix", "Netsparker", "Nstalker", "NTOSpider", "Skipfish", "W3af"]



for inputFile in inputFiles:
	fileName = inputFile + "/filterEvasions.txt"
	out = open(fileName, "w")
	templates = []
	payloadList = []
	requests = []
	templates, payloadList = xssTemplating.main(inputFile)
	#payloadList, requests = payloads.main(inputFile)

	#this is the list of possible filter evasions taken in consideration for the scanners (extracted from https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet)
	xssLocator = "false"
	caseInsensitive = "false"
	embedNewline = "false"
	embedNull = "false"
	defaultSrc = "false"
	divExpr = "false"
	iframe = "false"
	iframeBase64 = "false"
	bodyTag = "false"
	bbcode = "false"
	invalidSrc = "false"
	divBackground = "false"
	metaTag = "false"
	extraneousOpenBracket = "false"
	remoteStyleSheet = "false"
	malformedATag = "false"
	commentExpression = "false"
	embedTab = "false"
	embedEncodedTab = "false"
	fromCharCode = "false"
	
	
	#for key, value in templates.items():
		#for v in values:
	for v in payloadList:
		caseInsensitiveString = "false"
		out.write("PAYLOAD: " + v + "\n\n")
		
		testString = re.escape("';!--\"<XSS>=&{()}")
		testString = re.sub('XSS', '.*', testString)
		if re.search(testString ,v):
			out.write("xss Locator \n\n")
			xssLocator = "true"
	
		for scriptCase in re.findall("script", v, re.IGNORECASE):
			if scriptCase and not (scriptCase == scriptCase.lower()):
				caseInsensitive = "true"
				caseInsensitiveString = "true"

		for onErrorCase in re.findall("onerror", v, re.IGNORECASE):
			if onErrorCase and not (onErrorCase == onErrorCase.lower()):
				caseInsensitive = "true"
				caseInsensitiveString = "true"

		for jsCase in re.findall("javascript", v, re.IGNORECASE):
			if jsCase and not (jsCase == jsCase.lower()):
				caseInsensitive = "true"
				caseInsensitiveString = "true"

		if caseInsensitiveString == "true":
			out.write("case Insensitive \n\n")
		
		if ("\\r\\n" in v) or ("\\n" in v):
			out.write("embed Newline \n\n")
			embedNewline = "true"
		
		if "\\x00" in v:
			out.write("embed Null \n\n")
			embedNull = "true"
		
		if "img" in v.lower() and "src" not in v.lower():
			defaultSrc = "true"
			out.write("default Src \n\n")

		if "div" in v.lower() and "style" in v.lower() and "width" in v.lower() and "expression" in v.lower():
			out.write("div Expression \n\n")
			divExpr = "true"

		if "iframe" in v.lower():
			out.write("iframe \n\n")
			iframe = "true"

		if "iframe" in v.lower() and "base64" in v.lower():
			out.write("iframe and base 64 \n\n")
			iframeBase64 = "true"

		if "<body" in v.lower():
			out.write("body Tag \n\n")
			bodyTag = "true"

		if re.search("\[(.*)\](.*)\[(.*)\]" ,v):
			out.write("bbcode \n\n")
			bbcode = "true"

		if "src" in v.lower() and not (re.search("src=\".*?\"" ,v.lower(), re.S) or re.search("src=\'.*?\'" ,v.lower(), re.S)):
			out.write("invalid Src \n\n")
			invalidSrc = "true"

		if "div" in v.lower() and "style" in v.lower() and "background" in v.lower() and "javascript" in v.lower():
			out.write("div Background \n\n")
			divBackground = "true"

		if "meta" in v.lower() and "http-equiv" in v.lower() and "refresh" in v.lower() and "content" in v.lower() and "javascript" in v.lower():
			out.write("meta Tag \n\n")
			metaTag = "true"

		if "<<" in v:
			out.write("extraneous Open Bracket: \n\n")
			extraneousOpenBracket = "true"

		if "style" in v.lower() and "@import" in v.lower():
			out.write("remote StyleSheet \n\n")
			remoteStyleSheet = "true"

		if "<a" in v.lower() and not "href" in v.lower():
			out.write("malformed A Tag \n\n")
			malformedATag = "true"

		if "style" in v.lower() and "expression" not in v.lower():
			decomString = v
			while re.search("/\*", decomString) and re.search("\*/", decomString):
				startComment = re.search("/\*", decomString).start()
				endComment = re.search("\*/", decomString).end()
				decomString = decomString[0:startComment]+decomString[endComment:]
				if "expression" in decomString:
					out.write("comment Expression \n\n")
					commentExpression = "true"
					break

		if re.search("\\\\t" ,v):
			out.write("embed Tab \n\n")
			embedTab = "true"

		if "&#x09" in v.lower():
			out.write("embed Encoded Tab \n\n")
			embedEncodedTab = "true"

		if "String.fromCharCode" in v:
			out.write("fromCharCode \n\n")
			fromCharCode = "true"


			

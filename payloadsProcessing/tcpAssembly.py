#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import nids
import dpkt

end_states = (nids.NIDS_CLOSE, nids.NIDS_TIMEOUT, nids.NIDS_RESET)

def processTcpStream(tcp):
	((src, sport), (dst, dport)) = tcp.addr
	
	#print tcp.server.count
	# data to server
	server_data = tcp.server.data[:tcp.server.count]
	
	# data to client
        client_data = tcp.client.data[:tcp.client.count]

	# extract *all* the requests in this stream
	req = ""
	i = len(requests)
	'''
	inStreams = 0
	for s in streams:
		if str(server_data) == str(s):
			inStreams = 1
	if inStreams == 0:	
		streams.append(server_data)
	else:
		return
	'''
	out.write("\nStream:\n")
	out.write(str(server_data) + "\n\n")

	while len(req) < len(server_data):
		try:
			req = dpkt.http.Request(server_data)
			'''
			host_hdr = req.headers['host']
			full_uri = req.uri if req.uri.startswith("http://") else \
				"http://%s:%d%s" % (host_hdr, dport, req.uri) if dport != 80 else \
				"http://%s%s" % (host_hdr, req.uri)
			'''
			'''
			inReq = 0
			for r in requests:
				if str(req) == str(r):
					#print "REQ", req
					#print "R", r
					inReq = 1
			'''
			if not req in requests:
				requests.append(req)
				out.write("\nRichiesta accettata: " + str(i) + " \n")
				out.write(str(req) + "\n\n")
				i = i + 1
			'''
			if inReq == 0:
				requests.append(req)
				out.write("\nRichiesta accettata: " + str(i) + " \n")
				out.write(str(req) + "\n\n")
				i = i + 1
			'''
			#res = dpkt.http.Response(client_data)
			server_data = server_data[len(req):]
			
		except dpkt.UnpackError as e:
			print "\n", i
			print "\n", req
			print "Error: " + str(e)
			break



def handleTcpStream(tcp):
	((src, sport), (dst, dport)) = tcp.addr

	if tcp.nids_state == nids.NIDS_JUST_EST:
        
        	if dport in (80, 8000, 8080):
			tcp.client.collect = 1
			tcp.server.collect = 1

		openstreams[tcp.addr] = tcp

	elif tcp.nids_state == nids.NIDS_DATA:
		
		tcp.discard(0)
		openstreams[tcp.addr] = tcp 
    
	elif tcp.nids_state in end_states:
		del openstreams[tcp.addr]
		processTcpStream(tcp)

def main(f):
	
	
	nids.param("scan_num_hosts", 0)         # disable portscan detection

	nids.chksum_ctl([('0.0.0.0/0', False)]) # disable checksumming

	nids.param("device", None)

	nids.param("filename", f)

	nids.init()

	nids.register_tcp(handleTcpStream)
  

	# Loop until EOF (pcap file)
	try:
		nids.run()
		for c, stream in openstreams.items():
			#print c
			#out.write("Addr: " + str(c))
			processTcpStream(stream)

	except nids.error, e:
		print "nids/pcap error:", e
	except Exception, e:
        	print "misc. exception (runtime error in user callback?):", e
	
	return requests


requests = []
openstreams = {}
streams = []
out = open("prova.txt", "w")
#main(f)

'''
Ultimi test danno come risultati:
-Con tcp assembly:
Acunetix: 1818, trovate: 1700
Netsparker: 1674, trovate: 1669
NStalker: 545, trovate: 197
NTOSpider: 2586, trovate: 1556
Skipfish: 1148, trovate: 0
W3af: 882, trovate: 0

-soluzione vecchia
Acunetix: 1818, trovate: 1818
Netsparker: 1674, trovate: 1674 (1 ricostruita)
NStalker: 545, trovate: 545
NTOSpider: 2586, trovate: 2586 (75 ricostruite, 28 Frankenstein)
Skipfish: 1148, trovate: 1149
W3af: 882, trovate: 901 (19 Frankenstein)
'''
